import com.devcamp.javabasic_j01.s10.Circle;
import com.devcamp.javabasic_j01.s10.Cylinder;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.0);
        System.out.println(circle1);
        System.out.println(circle1.getArea());

        System.out.println(circle2);
        System.out.println(circle2.getArea());

        Cylinder cylinder1 = new Cylinder(2.5, 1.5);
        Cylinder cylinder2 = new Cylinder(1.5);
        Cylinder cylinder3 = new Cylinder(1.5, "green", 3.5);

        System.out.println(cylinder1);
        System.out.println(cylinder2);
        System.out.println(cylinder3);
        System.out.println(cylinder1.getVolume());
        System.out.println(cylinder2.getVolume());
        System.out.println(cylinder3.getVolume());

    }
}
