package com.devcamp.javabasic_j01.s10;

public class Cylinder extends Circle {
    private double height = 1.0;

    public Cylinder(double height) {
        this.height = height;
    }

    public Cylinder(double radious, double height) {
        super(radious);
        this.height = height;
    }

    public Cylinder(double radious, String color, double height) {
        super(radious, color);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getVolume() {
        return this.getArea() * this.height;
    }

}
